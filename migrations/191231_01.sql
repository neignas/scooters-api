-- +migrate Up

-- Scooter Occupation is not related with location sending events

ALTER TABLE `scooter_history_location`
    DROP FOREIGN KEY `scooter_history_location_occupy_id__scooter_history_occupy_id`;

ALTER TABLE `scooter_history_location`
    DROP INDEX `scooter_history_location_occupy_id__scooter_history_occupy_id`;

-- It's safe to rename column as it has no values
ALTER TABLE `scooter_history_location`
    CHANGE `occupy_id` `scooter_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `scooter_history_location`
    ADD CONSTRAINT `scooter_history_location_scooter_id__scooter_id` FOREIGN KEY (`scooter_id`) REFERENCES `scooter` (`id`);
