-- +migrate Up

CREATE TABLE IF NOT EXISTS `city`
(
    `id`   int(10) unsigned                     NOT NULL AUTO_INCREMENT,
    `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `user`
(
    `id`   int(10) unsigned                    NOT NULL AUTO_INCREMENT,
    `uuid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
    `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `uuid` (`uuid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `scooter`
(
    `id`      int(10) unsigned                    NOT NULL AUTO_INCREMENT,
    `uuid`    varchar(36) COLLATE utf8_unicode_ci NOT NULL,
    `city_id` int(10) unsigned                    NOT NULL,
    PRIMARY KEY (`id`),
    KEY `uuid` (`uuid`),
    KEY `scooter_city_id__city_id` (`city_id`),
    CONSTRAINT `scooter_city_id__city_id` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `scooter_history_occupy`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `scooter_id` int(10) unsigned NOT NULL,
    `user_id`    int(10) unsigned NOT NULL,
    `start_at`   datetime         NOT NULL,
    `end_at`     datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `scooter_history_occupy_scooter_id__scooter_id` (`scooter_id`),
    KEY `scooter_history_occupy_user_id__user_id` (`user_id`),
    CONSTRAINT `scooter_history_occupy__scooter_id` FOREIGN KEY (`scooter_id`) REFERENCES `scooter` (`id`),
    CONSTRAINT `scooter_history_occupy_user_id__user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `scooter_history_location`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `occupy_id`  int(10) unsigned NOT NULL,
    `lat`        decimal(10, 8)   NOT NULL,
    `lng`        decimal(11, 8)   NOT NULL,
    `created_at` datetime         NOT NULL,
    PRIMARY KEY (`id`),
    KEY `scooter_history_location_occupy_id__scooter_history_occupy_id` (`occupy_id`),
    CONSTRAINT `scooter_history_location_occupy_id__scooter_history_occupy_id` FOREIGN KEY (`occupy_id`) REFERENCES `scooter_history_occupy` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

-- It's possible to separate occupy and location data into separate tables for better data consistency and avoid possible
-- locks on table
CREATE TABLE IF NOT EXISTS `scooter_status`
(
    `id`              int(10) unsigned NOT NULL AUTO_INCREMENT,
    `scooter_id`      int(10) unsigned NOT NULL,
    `user_id`         int(10) unsigned DEFAULT NULL,
    `occupy_start_at` datetime         DEFAULT NULL,
    `occupy_end_at`   datetime         DEFAULT NULL,
    `geo_lat`         decimal(10, 8)   DEFAULT NULL,
    `geo_lng`         decimal(11, 8)   DEFAULT NULL,
    `geo_updated_at`  datetime         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `scooter_status_user_id__user_id` (`user_id`),
    KEY `scooter_status_scooter_id__scooter_id` (`scooter_id`),
    CONSTRAINT `scooter_status_scooter_id__scooter_id` FOREIGN KEY (`scooter_id`) REFERENCES `scooter` (`id`),
    CONSTRAINT `scooter_status_user_id__user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


INSERT INTO city(name)
VALUES ('Ottawa');
INSERT INTO city(name)
VALUES ('Montreal');
