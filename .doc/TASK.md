# Task for Scootin' Aboot API

## Description

A company called Scootin' Aboot will deploy electric scooters in Ottawa and Montreal. Design and implement a backend service that exposes a REST-like API intended for scooter event collecting and reporting to mobile clients.

1. The scooters report an event when a trip begins, report an event when the trip ends, and send in periodic updates on their location. After beginning a trip, the scooter is considered occupied. After a trip ends the scooter becomes free for use. A location update must contain the time, and geographical coordinates.
2. Mobile clients can query scooter locations and statuses in any rectangular location (e.g. two pair of coordinates), and filter them by status.
3. Both scooters and mobile client users can be identified by a UUID.
4. For the sake of simplicity, both mobile client apps and scooters can authenticate with the server using a static API key (i.e. no individual credentials necessary but will most probably be introduced as the project develops further).

## Deliverables

* API documentation in a reasonable format (has to be human readable, machine and human readable such as OpenAPI also great).
* Source code and build instructions for the application in a language (and framework) of choice.
* An example database dump.
* Any supplementary material you think is useful.
* Extra kudos if docker compose or a similar tool can be used to easily run the application.
* All of that in a private GitHub.com/GitLab.com/Bitbucket repository of your choice. We will provide an account to share it with.

## Tips

* Where spec is ambiguous make an assumption towards the simpler side or ask us. Document these assumptions made in an ASSUMPTIONS.md file. Simple bullet point list will be sufficient.
* Consider that many users could be using the application at the same time.
* Code quality will be considered, a good mix of eloquence, brevity, and proper organizational technique is appreciated.
* Meaningful tests are always very welcome.