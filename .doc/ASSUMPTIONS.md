# Scootin' Aboot Assumptions

Assumptions which I made while developing api.

## Assumptions
  - In task description there is mentioned that scooters will 
  be deployed in two cities. I decided to assign city reference to 
  scooter but did not used city in api business logic because there
  could be such assignment city->scooter to manage scooters by city
  but as they was deployed in Ottawa and Montreal so there is possibility
  to get from one city to other with same scooter. The better solution
  decide in which city scooter is located is describe coordinates (zones)
  for each city.
  - Every request to api should be made by providing apiKey url query 
  parameter which value could be found in Dockerfile.
  
  - Happy api flow:
    - Create user which will occupy scooter
    - Created user can occupy scooter.
    - After ride user will end scooter occupation
    - Scooter can send geo coordinates as a point of latitude/longitude
    - Clients can filter scooters locations and status in a rectangular
     location by providing two pair of coordinates. First coordinate
     top left corner point of rectangle (latitude, longitude), second
     point right bottom point of rectangle (latitude, longitude).
        - Status is an optional parameter, if not given api will return 
        scooters with any status in selected location.
    
## Tips
  - There are few comments in code which describes TODO needed implement
  simple logic tasks. Leaved them in TODO status as they are 
  mostly related with data compatibility, security and app speed.
  - There are only few tests which not cover all api features. In 
  case of little time decided did not spread with lot of tests.
  - ApiKey added to docker file then will be possible to easy have 
  apiKey value different in development environments.
  - In real life I will consider not use foreign keys in database 
  schema as they complicates database schema scaling process.
  - After all it was fun to play around :)
  