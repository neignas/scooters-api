package user

import (
	mysql "github.com/ignasne/scooters-api/database"
	"github.com/ignasne/scooters-api/logger"

	"github.com/sirupsen/logrus"
)

type Repository struct {
	DB mysql.ReaderWriter
}

type RepositoryI interface {
	Create(userName string) (int, error)
	FindOneByID(id int) (*User, error)
	FindOneByUuid(uuid string) (*User, error)
}

func (r *Repository) Create(userName string) (int, error) {
	res, err := r.DB.Exec(`
		INSERT INTO user (uuid, name)
		VALUES (UUID(), ?)`, userName)

	if err != nil {
		logger.Get().WithError(err).
			WithFields(logrus.Fields{"user": userName}).
			Warn("could not create user")
		return 0, err
	}

	lastID, _ := res.LastInsertId()

	return int(lastID), nil
}

func (r *Repository) FindOneByID(id int) (*User, error) {
	row := r.DB.QueryRow(`
	SELECT id
		, uuid
		, name
	FROM user
	WHERE 
		id = ?`, id)

	user := User{}

	err := row.Scan(
		&user.ID,
		&user.UUID,
		&user.Name,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not fetch user")
		return nil, err
	}

	return &user, nil
}

func (r *Repository) FindOneByUuid(uuid string) (*User, error) {
	row := r.DB.QueryRow(`
	SELECT id
		, uuid
		, name
	FROM user
	WHERE 
		uuid = ?`, uuid)

	user := User{}

	err := row.Scan(
		&user.ID,
		&user.UUID,
		&user.Name,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not fetch user")
		return nil, err
	}

	return &user, nil
}
