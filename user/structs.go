package user

type User struct {
	ID   int
	UUID string
	Name string
}

type Data struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}
