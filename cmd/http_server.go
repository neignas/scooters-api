package cmd

import (
	"github.com/ignasne/scooters-api/api"
	"github.com/ignasne/scooters-api/config"
	mysql "github.com/ignasne/scooters-api/database"
	"github.com/ignasne/scooters-api/router"
	"github.com/ignasne/scooters-api/scooter"
	"github.com/ignasne/scooters-api/user"
	"github.com/spf13/cobra"
)

var httpServerCmd = &cobra.Command{
	Use:   "server",
	Short: "Run HTTP server",
	Run: func(cmd *cobra.Command, args []string) {
		runHttpServer()
	},
}

func init() {
	rootCmd.AddCommand(httpServerCmd)
}

func runHttpServer() {
	cfg := &config.Main{}
	cfg.Parse()

	db := mysql.Connect(cfg.DB)

	httpAPI := api.New(cfg.SelfPort)
	router := router.New(httpAPI.Mux)

	router.RegisterUserRoutes(&user.Repository{DB: db})
	router.RegisterScooterRoutes(&scooter.Repository{DB: db}, &user.Repository{DB: db})

	httpAPI.RegisterRoutes()
	httpAPI.Listen()
}
