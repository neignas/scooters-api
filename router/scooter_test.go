package router

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	gbl "github.com/franela/goblin"
	"github.com/ignasne/scooters-api/scooter"
	"github.com/ignasne/scooters-api/user"
	. "github.com/onsi/gomega"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type ScootersRepoMock struct{}
type UsersRepoMock struct{}

func (m ScootersRepoMock) FindAllByLocationAndStatus(latLeft string, latRight string, lngBottom string, lngTop string, status scooter.StatusType) ([]*scooter.Scooter, error) {
	scooterStatuses := make([]*scooter.Scooter, 0)

	scooterStatus := &scooter.Scooter{
		ID:            1,
		UUID:          "5b842fa3-2bc4-11ea-88a2-0242ac130002",
		Latitude:      "54.72140",
		Longitude:     "25.246090",
		Status:        "free",
		OccupyStartAt: nil,
	}

	scooterStatuses = append(scooterStatuses, scooterStatus)

	return scooterStatuses, nil
}

func (m ScootersRepoMock) LocationUpdate(scooterId int, lat string, lng string, sendAt string) error {
	return nil
}

func (m ScootersRepoMock) OccupyStart(userId int, scooterId int) (string, error) {
	return "2020-01-03 10:50:00", nil
}

func (m ScootersRepoMock) OccupyEnd(userId int, scooterId int) (string, error) {
	return "2020-01-03 10:55:00", nil
}

func (m ScootersRepoMock) IsOccupied(scooterId int) (bool, error) {
	return false, nil
}

func (m ScootersRepoMock) FindOneByUuid(uuid string) (*scooter.Scooter, error) {
	s := &scooter.Scooter{
		ID: 1,
		UUID: "5b842fa3-2bc4-11ea-88a2-0242ac130002",
	}
	return s, nil
}

func (m ScootersRepoMock) StartTransaction() (*sql.Tx, error) {
	return nil, nil
}

func (u UsersRepoMock) Create(userName string) (int, error) {
	return 1, nil
}

func (u UsersRepoMock) FindOneByID(id int) (*user.User, error) {
	return nil, nil
}

func (u UsersRepoMock) FindOneByUuid(uuid string) (*user.User, error) {
	user := &user.User{
		ID:   2,
		UUID: "908ab9cd-2db3-11ea-88a2-0242ac130002",
		Name: "Floky",
	}
	return user, nil
}

func TestScooterEndpoint(t *testing.T) {
	g := gbl.Goblin(t)

	repositoryMock := &ScootersRepoMock{}
	userRepositoryMock := &UsersRepoMock{}

	RegisterFailHandler(func(m string, _ ...int) { g.Fail(m) })

	g.Describe("Occupy", func() {

		g.It("should let occupy scooter for user", func() {
			occupyData := &scooter.OccupyData{
				UserUUID: "6974a3d7-2bc4-11ea-88a2-0242ac130002",
				ScooterUUID: "5b842fa3-2bc4-11ea-88a2-0242ac130002",
			}
			occupyJson, _ := json.Marshal(occupyData)

			req, _ := http.NewRequest(http.MethodPost, "/scooter/occupy", bytes.NewBuffer(occupyJson))
			res := httptest.NewRecorder()

			r := scooterRouter{
				Repository: repositoryMock,
				UserRepository: userRepositoryMock,
			}

			r.occupyStartHandler().ServeHTTP(res, req)

			bodyBytes, _ := ioutil.ReadAll(res.Body)
			bodyString := string(bodyBytes)

			expected := []byte(`{"data":{"occupiedAt":"2020-01-03 10:50:00","occupiedEndAt":"","scooterUuid":"5b842fa3-2bc4-11ea-88a2-0242ac130002"}}`)

			Expect(res.Code).To(Equal(200))
			Expect(bodyString).To(Equal(string(expected)))
		})

		g.It("should let end scooter occupation", func() {
			occupyData := &scooter.OccupyData{
				UserUUID: "6974a3d7-2bc4-11ea-88a2-0242ac130002",
				ScooterUUID: "5b842fa3-2bc4-11ea-88a2-0242ac130002",
			}
			occupyJson, _ := json.Marshal(occupyData)

			req, _ := http.NewRequest(http.MethodPut, "/scooter/occupy", bytes.NewBuffer(occupyJson))
			res := httptest.NewRecorder()

			r := scooterRouter{
				Repository: repositoryMock,
				UserRepository: userRepositoryMock,
			}

			r.occupyEndHandler().ServeHTTP(res, req)

			bodyBytes, _ := ioutil.ReadAll(res.Body)
			bodyString := string(bodyBytes)

			expected := []byte(`{"data":{"occupiedAt":"","occupiedEndAt":"2020-01-03 10:55:00","scooterUuid":"5b842fa3-2bc4-11ea-88a2-0242ac130002"}}`)

			Expect(res.Code).To(Equal(200))
			Expect(bodyString).To(Equal(string(expected)))
		})

		g.It("should be able to filter scooters", func() {
			vars := map[string]string{
				"pointTopLeft[lat]": "54.72140",
				"pointTopLeft[lng]": "25.246090",
				"pointBottomRight[lat]": "54.719585",
				"pointBottomRight[lng]": "25.250940",
				"status": "all",
			}

			url := "/scooter/location?"

			for varKey, varValue := range vars {
				url = fmt.Sprintf("%s&%s=%s", url, varKey, varValue)
			}

			req, _ := http.NewRequest(http.MethodGet, url, nil)
			res := httptest.NewRecorder()

			r := scooterRouter{
				Repository: repositoryMock,
				UserRepository: userRepositoryMock,
			}

			r.locationIndexHandler().ServeHTTP(res, req)

			bodyBytes, _ := ioutil.ReadAll(res.Body)
			bodyString := string(bodyBytes)

			expected := []byte(`{"data":{"scooters":[{"ID":1,"UUID":"5b842fa3-2bc4-11ea-88a2-0242ac130002","Latitude":"54.72140","Longitude":"25.246090","Status":"free","OccupyStartAt":null}]}}`)

			Expect(res.Code).To(Equal(200))
			Expect(bodyString).To(Equal(string(expected)))
		})

		g.It("should update scooter location", func() {
			scooterLocation := &scooter.LocationData{
				ScooterUUID: "5b842fa3-2bc4-11ea-88a2-0242ac130002",
				Latitude: "54.720471",
				Longitude: "25.247818",
				DateAt: "2019-12-31 11:58:51",
			}
			locationJson, _ := json.Marshal(scooterLocation)

			req, _ := http.NewRequest(http.MethodPost, "/scooter/location", bytes.NewBuffer(locationJson))
			res := httptest.NewRecorder()

			r := scooterRouter{
				Repository: repositoryMock,
				UserRepository: userRepositoryMock,
			}

			r.locationPostHandler().ServeHTTP(res, req)

			bodyBytes, _ := ioutil.ReadAll(res.Body)
			bodyString := string(bodyBytes)

			expected := []byte(`{"data":"ok"}`)

			Expect(res.Code).To(Equal(200))
			Expect(bodyString).To(Equal(string(expected)))
		})
	})
}
