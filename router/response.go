package router

import (
	resp "github.com/nicklaw5/go-respond"
	"net/http"
)

type ResourceResponse struct {
	Data  interface{} `json:"data,omitempty"`
	Error error       `json:"errors,omitempty"`
}

func BadRequest(r http.ResponseWriter, err error) {
	resp.NewResponse(r).BadRequest(&ResourceResponse{Error: err})
}

func Ok(r http.ResponseWriter, v interface{}) {
	resp.NewResponse(r).Ok(&ResourceResponse{Data: v})
}

func ServerError(r http.ResponseWriter, err error) {
	resp.NewResponse(r).InternalServerError(&ResourceResponse{Error: err})
}

func NotFound(r http.ResponseWriter, err error) {
	resp.NewResponse(r).NotFound(&ResourceResponse{Error: err})
}

func Conflict(r http.ResponseWriter, err error) {
	resp.NewResponse(r).Conflict(&ResourceResponse{Error: err})
}

func Unauthorized(r http.ResponseWriter, err error) {
	resp.NewResponse(r).Unauthorized(&ResourceResponse{Error: err})
}
