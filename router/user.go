package router

import (
	"encoding/json"
	"github.com/ignasne/scooters-api/errors"
	"github.com/ignasne/scooters-api/logger"
	"github.com/ignasne/scooters-api/user"
	"net/http"
)

type userRouter struct {
	Repository *user.Repository
}

func (r *Router) RegisterUserRoutes(repository *user.Repository) {
	router := &userRouter{
		Repository: repository,
	}

	mr := r.Router.PathPrefix("/user").Subrouter()

	mr.HandleFunc("", router.postHandler()).Methods("POST")
}

func (r *userRouter) postHandler() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

		var data user.Data
		err := json.NewDecoder(req.Body).Decode(&data)

		if err != nil {
			logger.Get().WithError(err).Warn("could not parse body json")
			BadRequest(res, err)
			return
		}

		// user name is not unique, so no need to check for duplicates
		userId, err := r.Repository.Create(data.Name)

		if err != nil {
			ServerError(res, err)
			return
		}

		userData, err := r.Repository.FindOneByID(userId)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get User data")
			ServerError(res, err)
			return
		}

		if userData == nil {
			NotFound(res, errors.New("User not found"))
			return
		}

		responseData := user.Data{UUID: userData.UUID, Name: userData.Name}

		Ok(res, responseData)
	}
}
