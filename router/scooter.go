package router

import (
	"encoding/json"
	"fmt"
	"github.com/gofrs/uuid"
	"github.com/ignasne/scooters-api/errors"
	"github.com/ignasne/scooters-api/helpers"
	"github.com/ignasne/scooters-api/logger"
	"github.com/ignasne/scooters-api/scooter"
	"github.com/ignasne/scooters-api/user"
	"net/http"
)

type scooterRouter struct {
	Repository     scooter.RepositoryI
	UserRepository user.RepositoryI
}

func (r *Router) RegisterScooterRoutes(repository scooter.RepositoryI, userRepository user.RepositoryI) {
	router := &scooterRouter{
		Repository:     repository,
		UserRepository: userRepository,
	}

	mr := r.Router.PathPrefix("/scooter").Subrouter()

	mr.HandleFunc("/occupy", router.occupyStartHandler()).Methods("POST")
	mr.HandleFunc("/occupy", router.occupyEndHandler()).Methods("PUT")
	mr.HandleFunc("/location", router.locationIndexHandler()).Methods("GET")
	mr.HandleFunc("/location", router.locationPostHandler()).Methods("POST")
}

func (r *scooterRouter) locationIndexHandler() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		queryParams := req.URL.Query()

		pointTopLeftLat := queryParams["pointTopLeft[lat]"]
		pointTopLeftLng := queryParams["pointTopLeft[lng]"]

		pointBottomRightLat := queryParams["pointBottomRight[lat]"]
		pointBottomRightLng := queryParams["pointBottomRight[lng]"]
		status := queryParams["status"]
		statusType := scooter.All

		if helpers.ContainsEmptyArraysOfStrings(pointTopLeftLat, pointTopLeftLng, pointBottomRightLat, pointBottomRightLng) {
			BadRequest(res, errors.New("Please provide correct coordinates for pointTopLeft and pointBottomRight parameters."))
			return
		}

		if len(status) > 0 {
			statusType = scooter.StatusType(status[0])
		}

		if	scooter.ValidateStatusType(statusType) == false {
			BadRequest(res, errors.New("Bad value of status parameter."))
			return
		}

		locationController := &scooter.LocationController{
			Repository: r.Repository,
		}

		response, err := locationController.Get(
			pointTopLeftLat[0],
			pointTopLeftLng[0],
			pointBottomRightLat[0],
			pointBottomRightLng[0],
			statusType)

		if err != nil {
			ServerError(res, err)
			return
		}

		Ok(res, response)
	}
}

func (r *scooterRouter) locationPostHandler() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

		// TODO move out code from routers to controllers
		var data scooter.LocationData
		err := json.NewDecoder(req.Body).Decode(&data)

		if err != nil {
			logger.Get().WithError(err).Warn("could not parse body json")
			BadRequest(res, err)
			return
		}

		_, scooterUuidErr := uuid.FromString(data.ScooterUUID)
		if scooterUuidErr != nil {
			logger.Get().WithError(err).Warn(fmt.Printf(
				"could not parse uuid. ScooterUuid: %q. Error: %v",
				data.ScooterUUID,
				err))
			BadRequest(res, err)
		}

		scooterData, err := r.Repository.FindOneByUuid(data.ScooterUUID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get scooter data")
			ServerError(res, err)
			return
		}

		timeCreated, err := helpers.TimeFromString(data.DateAt)

		if err != nil {
			ServerError(res, err)
		}

		// TODO validate geo coordinates
		err = r.Repository.LocationUpdate(scooterData.ID, data.Latitude, data.Longitude, helpers.TimeToString(timeCreated))

		if err != nil {
			ServerError(res, err)
			return
		}

		Ok(res, "ok")
	}
}

func (r *scooterRouter) occupyStartHandler() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {

		response := scooter.OccupyResponse{}
		var data scooter.OccupyData
		err := json.NewDecoder(req.Body).Decode(&data)

		if err != nil {
			logger.Get().WithError(err).Warn("could not parse body json")
			BadRequest(res, err)
			return
		}

		_, scooterUuidErr := uuid.FromString(data.ScooterUUID)
		_, userUuidErr := uuid.FromString(data.UserUUID)
		if scooterUuidErr != nil || userUuidErr != nil {
			logger.Get().WithError(err).Warn(fmt.Printf(
				"could not parse uuid. UserUuid: %q: ScooterUuid: %q. Error: %v",
				data.UserUUID,
				data.ScooterUUID,
				err))
			BadRequest(res, err)
		}

		// TODO check if scooter exist
		scooterData, err := r.Repository.FindOneByUuid(data.ScooterUUID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get scooter data")
			ServerError(res, err)
			return
		}

		userData, err := r.UserRepository.FindOneByUuid(data.UserUUID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get user data")
			ServerError(res, err)
			return
		}
		// check if scooter not occupied
		scooterOccupied, err := r.Repository.IsOccupied(scooterData.ID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get scooter status data")
			ServerError(res, err)
			return
		}

		if scooterOccupied {
			Conflict(res, err)
			return
		}

		occupyStartedAt, err := r.Repository.OccupyStart(userData.ID, scooterData.ID)

		if err != nil {
			ServerError(res, err)
			return
		}

		response.ScooterUUID = data.ScooterUUID
		response.OccupiedAt = occupyStartedAt

		Ok(res, response)
	}
}

func (r *scooterRouter) occupyEndHandler() http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		response := scooter.OccupyResponse{}
		var data scooter.OccupyData
		err := json.NewDecoder(req.Body).Decode(&data)

		if err != nil {
			logger.Get().WithError(err).Warn("could not parse body json")
			BadRequest(res, err)
			return
		}

		// TODO extract this code to separate func as it's repeating
		_, scooterUuidErr := uuid.FromString(data.ScooterUUID)
		_, userUuidErr := uuid.FromString(data.UserUUID)
		if scooterUuidErr != nil || userUuidErr != nil {
			logger.Get().WithError(err).Warn(fmt.Printf(
				"could not parse uuid. UserUuid: %q: ScooterUuid: %q. Error: %v",
				data.UserUUID,
				data.ScooterUUID,
				err))
			BadRequest(res, err)
		}

		// TODO check scooter exists
		scooterData, err := r.Repository.FindOneByUuid(data.ScooterUUID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get scooter data")
			ServerError(res, err)
			return
		}

		userData, err := r.UserRepository.FindOneByUuid(data.UserUUID)

		if err != nil {
			logger.Get().WithError(err).Error("Could not get user data")
			ServerError(res, err)
			return
		}

		occupyEndAt, err := r.Repository.OccupyEnd(userData.ID, scooterData.ID)

		if err != nil {
			ServerError(res, err)
			return
		}

		response.ScooterUUID = data.ScooterUUID
		response.OccupiedEndAt = occupyEndAt

		Ok(res, response)
	}
}
