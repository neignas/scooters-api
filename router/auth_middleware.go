package router

import (
	"github.com/caarlos0/env"
	"github.com/ignasne/scooters-api/errors"
	"github.com/ignasne/scooters-api/logger"
	"net/http"
	"os"
)

type ApiKey struct {
	Value string `env:"API_KEY,required"`
}

// TODO use interface
func GetAuthMiddleware() *AuthMiddleware {
	api := &ApiKey{}
	err := env.Parse(api)
	if err != nil {
		logger.Get().WithError(err).Fatal("Could not parse env for apiKey value")
		os.Exit(1)
	}

	auth := &AuthMiddleware{ApiKey:api}

	return auth
}

func (a *ApiKey) Verify(value string) bool {
	return value == a.Value
}

type AuthMiddleware struct {
	ApiKey *ApiKey
}

func (m *AuthMiddleware) middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		queryParams := req.URL.Query()

		if len(queryParams["apiKey"]) == 0 {
			Unauthorized(w, errors.New("Please provide apiKey for Authorization."))
			return
		}

		apiKey := queryParams["apiKey"][0]

		success := m.ApiKey.Verify(apiKey)

		if success == false {
			Unauthorized(w, errors.New("Invalid apiKey provided"))
			return
		}

		h.ServeHTTP(w, req)
	})
}
