package helpers

import (
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

func Timestamp() string {
	// TODO LoadLocation could be used for specific time zone
	return time.Now().UTC().Format(timeFormat)
}

func TimeFromString(timeData string) (time.Time, error) {
	return time.Parse(timeFormat, timeData)
}

func TimeToString(time time.Time) string {
	return time.Format(timeFormat)
}

func ContainsEmptyArraysOfStrings(arr ...[]string) bool {
	for _, s := range arr {
		if len(s) == 0 {
			return true
		}
	}
	return false
}