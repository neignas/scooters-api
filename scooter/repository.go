package scooter

import (
	"bytes"
	"database/sql"
	mysql "github.com/ignasne/scooters-api/database"
	"github.com/ignasne/scooters-api/helpers"
	"github.com/ignasne/scooters-api/logger"
)

type Repository struct {
	DB mysql.ReaderWriter
}

type RepositoryI interface {
	FindAllByLocationAndStatus(latLeft string, latRight string, lngBottom string, lngTop string, status StatusType) ([]*Scooter, error)
	LocationUpdate(scooterId int, lat string, lng string, sendAt string) error
	OccupyStart(userId int, scooterId int) (string, error)
	OccupyEnd(userId int, scooterId int) (string, error)
	IsOccupied(scooterId int) (bool, error)
	FindOneByUuid(uuid string) (*Scooter, error)
	StartTransaction() (*sql.Tx, error)
}

func (r *Repository) StartTransaction() (*sql.Tx, error) {
	return r.DB.Begin()
}

func (r *Repository) FindAllByLocationAndStatus(latLeft string, latRight string, lngBottom string, lngTop string, status StatusType) ([]*Scooter, error) {
	queryString := `SELECT
					  s.uuid
					, ss.geo_lat
					, ss.geo_lng
					, ss.occupy_start_at
				FROM
					scooter_status ss
				INNER JOIN
					scooter s
					ON s.id = ss.scooter_id
				WHERE
					(geo_lat BETWEEN ? AND ?)
				AND
					(geo_lng BETWEEN ? AND ?)`

	var buffer bytes.Buffer

	buffer.WriteString(queryString)

	if status != All {
		buffer.WriteString(" AND ")

		switch status {
		case Free:
			buffer.WriteString(" ss.occupy_start_at IS NULL ")
		case Taken:
			buffer.WriteString(" ss.occupy_start_at IS NOT NULL ")
		}
	}

	rows, err := r.DB.Query(
		buffer.String(),
		latLeft,
		latRight,
		lngBottom,
		lngTop,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not fetch scooter statuses")
		return nil, err
	}
	defer rows.Close()

	scooterStatuses := make([]*Scooter, 0)

	for rows.Next() {
		scooter := Scooter{}

		err := rows.Scan(
			&scooter.UUID,
			&scooter.Latitude,
			&scooter.Longitude,
			&scooter.OccupyStartAt,
		)

		if err != nil {
			return nil, err
		}

		scooter.Status = Free

		if scooter.OccupyStartAt != nil {
			scooter.Status = Taken
		}

		scooterStatuses = append(scooterStatuses, &scooter)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return scooterStatuses, nil
}

func (r *Repository) LocationUpdate(scooterId int, lat string, lng string, sendAt string) error {
	tx, err := r.StartTransaction()
	defer tx.Rollback()

	if err != nil {
		logger.Get().WithError(err).Error("could not start sql transaction")
		return err
	}
	// firstly update scooter location in status data as it's actual data
	queryString := `UPDATE scooter_status SET geo_lat = ?, geo_lng = ?, geo_updated_at = ? WHERE scooter_id = ?`

	// TODO check if scooter status row exists and throw error on not existing scooter_status row

	_, err = tx.Exec(
		queryString,
		lat,
		lng,
		sendAt,
		scooterId,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not update scooter coordinates")
		return err
	}

	// add scooter occupy history
	queryString = `INSERT INTO scooter_history_location (scooter_id, lat, lng, created_at)
		VALUES (?, ?, ?, ?)`

	_, err = tx.Exec(
		queryString,
		scooterId,
		lat,
		lng,
		sendAt,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not add scooter location history data")
		return err
	}

	_ = tx.Commit()

	return nil
}

func (r *Repository) OccupyStart(userId int, scooterId int) (string, error) {
	tx, err := r.StartTransaction()
	defer tx.Rollback()

	if err != nil {
		logger.Get().WithError(err).Error("could not start sql transaction")
		return "", err
	}

	// firstly update scooter status as it's actual data
	queryString := `UPDATE scooter_status SET user_id = ?, occupy_start_at = ?, occupy_end_at = NULL WHERE scooter_id = ?`

	occupyStartAt := helpers.Timestamp()

	// TODO check if scooter status row exists and throw error on not existing scooter_status row

	_, err = tx.Exec(
		queryString,
		userId,
		occupyStartAt,
		scooterId,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not start scooter occupation")
		return "", err
	}

	// add scooter occupy history
	queryString = `INSERT INTO scooter_history_occupy (scooter_id, user_id, start_at)
		VALUES (?, ?, ?)`

	_, err = tx.Exec(
		queryString,
		scooterId,
		userId,
		occupyStartAt,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not add scooter occupy history data")
		return "", err
	}

	_ = tx.Commit()

	return occupyStartAt, nil
}

func (r *Repository) OccupyEnd(userId int, scooterId int) (string, error) {
	tx, err := r.StartTransaction()
	defer tx.Rollback()

	if err != nil {
		logger.Get().WithError(err).Error("could not start sql transaction")
		return "", err
	}

	// firstly update scooter status as this data is actual one
	// TODO check that scooter_id and user_id match given for data consistency
	queryString := `UPDATE scooter_status SET user_id = NULL, occupy_start_at = NULL WHERE scooter_id = ? AND user_id = ?`

	occupyEndAt := helpers.Timestamp()

	// TODO check if scooter status row exists and throw error on not existing scooter_status row

	_, err = tx.Exec(
		queryString,
		scooterId,
		userId,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not finish scooter occupation")
		return "", err
	}

	// todo check data consistency e.g. that there is only one scooter history occupy row with same scooter and user ids
	// todo -- and endAt with null value
	// update scooter occupy history
	queryString = `UPDATE scooter_history_occupy SET end_at = ? WHERE scooter_id = ? AND user_id = ? AND end_at IS NULL`

	_, err = tx.Exec(
		queryString,
		occupyEndAt,
		scooterId,
		userId,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not update scooter history data")
		return "", err
	}

	_ = tx.Commit()

	return occupyEndAt, nil
}

func (r *Repository) IsOccupied(scooterId int) (bool, error) {
	row := r.DB.QueryRow(`
	SELECT occupy_start_at
	FROM scooter_status
	WHERE 
		scooter_id = ?`, scooterId)

	occupation := Occupation{}

	err := row.Scan(
		&occupation.StartAt,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not fetch scooter occupy data")
		return false, err
	}

	occupied := false

	if occupation.StartAt != nil {
		occupied = true
	}

	return occupied, nil
}

func (r *Repository) FindOneByUuid(uuid string) (*Scooter, error) {
	row := r.DB.QueryRow(`
	SELECT id
		, uuid
	FROM scooter
	WHERE 
		uuid = ?`, uuid)

	scooter := Scooter{}

	err := row.Scan(
		&scooter.ID,
		&scooter.UUID,
	)

	if err != nil {
		logger.Get().WithError(err).Error("could not fetch scooter")
		return nil, err
	}

	return &scooter, nil
}
