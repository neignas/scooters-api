package scooter

import "time"

type Scooter struct {
	ID            int
	UUID          string
	Latitude      string
	Longitude     string
	Status        StatusType
	OccupyStartAt *time.Time
}

type Occupation struct {
	ID        int
	ScooterID int
	UserID    int
	StartAt   *time.Time
	EndAt     *time.Time
}

type Data struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
	City string `json:"cityName"`
}

type OccupyData struct {
	UserUUID    string `json:"userUuid"`
	ScooterUUID string `json:"scooterUuid"`
}

type OccupyResponse struct {
	OccupiedAt    string `json:"occupiedAt"`
	OccupiedEndAt string `json:"occupiedEndAt"`
	ScooterUUID   string `json:"scooterUuid"`
}

type LocationData struct {
	ScooterUUID string `json:"scooterUuid"`
	Latitude    string `json:"latitude"`
	Longitude   string `json:"longitude"`
	DateAt      string `json:"dateAt"`
}

type LocationResponse struct {
	Scooters []*Scooter `json:"scooters"`
}
