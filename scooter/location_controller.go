package scooter

import (
	"github.com/shopspring/decimal"
)

type LocationController struct {
	Repository RepositoryI
}

func (c *LocationController) Get(
	pointTopLeftLat string,
	pointTopLeftLng string,
	pointBottomRightLat string,
	pointBottomRightLng string,
	status StatusType) (*LocationResponse, error) {

	// TODO validate geo coordinates. Split coordinate by dot(.) and validate 180 / -180 values

	// compare coordinates leftLat with rightLat and leftLng with RightLng to pass first pointTopLeftLat
	// with smaller value as mysql in BETWEEN using it and coordinates should be like BETWEEN (smaller AND bigger) values

	leftLat := pointTopLeftLat
	rightLat := pointBottomRightLat
	leftLng := pointTopLeftLng
	rightLng := pointBottomRightLng

	topLeftLat, err := decimal.NewFromString(pointTopLeftLat)
	if err != nil {
		return nil, err
	}

	bottomRightLat, err := decimal.NewFromString(pointBottomRightLat)
	if err != nil {
		return nil, err
	}

	// it means that first pointTopLeftLat is bigger than pointBottomRightLat and need to be switched
	if topLeftLat.Cmp(bottomRightLat) == 1 {
		leftLat = pointBottomRightLat
		rightLat = pointTopLeftLat
	}

	topLeftLng, err := decimal.NewFromString(pointTopLeftLng)
	if err != nil {
		return nil, err
	}

	bottomRightLng, err := decimal.NewFromString(pointBottomRightLng)
	if err != nil {
		return nil, err
	}

	// it means that first pointBottomRightLng is bigger than pointTopLeftLng and need to be switched
	if topLeftLng.Cmp(bottomRightLng) == 1 {
		leftLng = pointBottomRightLng
		rightLng = pointTopLeftLng
	}

	scooterList, err := c.Repository.FindAllByLocationAndStatus(leftLat, rightLat, leftLng, rightLng, status)

	if err != nil {
		return nil, err
	}

	response := &LocationResponse{
		Scooters: scooterList,
	}

	return response, nil
}
