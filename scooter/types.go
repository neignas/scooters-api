package scooter

type StatusType string

const (
	Taken StatusType = "taken"
	Free  StatusType = "free"
	All   StatusType = "all"
)

func ValidateStatusType(statusValue StatusType) bool {
	switch statusValue {
	case Free, Taken, All:
		return true
	}

	return false
}
