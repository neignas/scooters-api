package errors

import "fmt"

type Error struct {
	Message string `json:"message"`
}

func New(message string) Error {
	err := Error{
		Message: message,
	}

	return err
}

func (e Error) Error() string {
	return fmt.Sprintf("[message]: %s", e.Message)
}