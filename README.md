# Scootin' Aboot API

API for managing scooters rental.

## Overview

 - Task description could be found in [TASK.md](.doc/TASK.md)
 - Assumptions stick in [ASSUMPTIONS.md](.doc/ASSUMPTIONS.md)
 - API documentation lies in [swagger.yaml](.doc/swagger.yaml)

## Running API
Using [Makefile](https://en.wikipedia.org/wiki/Makefile)

### To run
```bash
make build && make start
```
Api docker containers will be created and running. There are
database migrations which should be executed manually.
Locate api docker container while executing:
```bash
docker ps
```
Search for `scooters-api` image and copy `CONTAINER ID`

Login to docker container:
```bash
docker exec -it CONTAINER_ID bin/sh
```

Run the following command inside `scooters-api` container:
```bash
scooters-api migrate
```

Successful result should output message similar to this one:
```
{"level":"info","msg":"migrations applied","time":"2020-01-04T20:50:56Z","total_migrations":3}
```

For stopping Docker containers:
```bash
make stop
```

### For development
1. Update vendors:
    ```bash
    make vendor
    ```
1. Run migrations:
    ```bash
    make migrate
    ```
1. Run api:
    ```bash
    make run-dev
    ```