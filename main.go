package main

import "github.com/ignasne/scooters-api/cmd"

func main() {
	cmd.Execute()
}