package config

import (
	"github.com/ignasne/scooters-api/logger"
	"os"

	"github.com/caarlos0/env"
)

type Migration struct {
	// @migrate dockerfile
	Path string `env:"APP_MIGRATIONS_PATH" envDefault:"migrations"`
}

func GetMigration() *Migration {
	c := &Migration{}

	err := env.Parse(c)
	if err != nil {
		logger.Get().WithError(err).Fatal("could not parse migration config")
		os.Exit(1)
	}

	return c
}
