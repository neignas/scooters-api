package config

import (
	"github.com/ignasne/scooters-api/logger"
	"os"

	"github.com/caarlos0/env"
)

func (c *Main) Parse() {
	c.DB = GetDB()

	err := env.Parse(c)
	if err != nil {
		logger.Get().WithError(err).Fatal("Could not parse Config")
		os.Exit(1)
	}
}
