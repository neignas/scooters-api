module github.com/ignasne/scooters-api

go 1.12

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/casbin/casbin v1.9.1 // indirect
	github.com/franela/goblin v0.0.0-20181003173013-ead4ad1d2727
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/nicklaw5/go-respond v0.0.0-20190722175312-54f5cd3d2246
	github.com/onsi/gomega v1.8.1
	github.com/rs/cors v1.7.0
	github.com/rubenv/sql-migrate v0.0.0-20191213152630-06338513c237
	github.com/shopspring/decimal v0.0.0-20191130220710-360f2bc03045
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
)
